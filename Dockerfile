FROM adoptopenjdk/openjdk11:alpine-slim

COPY /target/savings.jar /srv/savings/savings.jar
WORKDIR /srv/savings
ENTRYPOINT ["java","-agentlib:jdwp=transport=dt_socket,address=*:5005,server=y,suspend=n","-Djava.security.egd=file:/dev/./urandom","-jar","savings.jar"]
