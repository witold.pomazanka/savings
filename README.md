# Savings

This application is created for storings savings and expenditures.

### Development mode:
Prerequisites: [docker] and [docker-compose]

1) **Database:**    
 run: `docker-compose up -d` command in `config/development` dir

2) **Backend application:**     
 Execute command in cmd/PowerShell - in source directory: `mvn clean install` or run directly from IDE; to compile sources before run   
 Execute command in cmd/PowerShell - in source directory: `mvn spring-boot:run` or run directly from IDE   

3) **Frontend/UI**          
 Proceed to [savings-ui] README.md

### Pushing changes:
Pushing to **master** branch is disabled. To push changes it is necessary to create feature branch and create merge request on bitbucket projects page. 

After successful merge, bitbucket will take care of creating new version of an application and will push it in docker registry - only if all tests were passed.

[docker]: https://docs.docker.com/get-docker

[docker-compose]: https://docs.docker.com/compose/install

[savings-ui]: https://gitlab.com/witold.pomazanka/savings-ui