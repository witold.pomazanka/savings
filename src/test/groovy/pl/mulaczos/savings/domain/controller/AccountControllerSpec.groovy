package pl.mulaczos.savings.domain.controller

import pl.mulaczos.savings.domain.base.BaseContextSpec

class AccountControllerSpec extends BaseContextSpec {

    def "Should return default account"() {
        when:
        def response = getDefaultAccountAsMap()

        then:
        response.name == 'DEFAULT'
        response.id == 1
        response.balance == 555
        response.defaultAccount
    }
}
