package pl.mulaczos.savings.domain.controller

import org.springframework.security.test.context.support.WithMockUser
import org.springframework.transaction.annotation.Transactional
import pl.mulaczos.savings.domain.SharedExpendituresSpec
import pl.mulaczos.savings.domain.dto.AddExpenditureDto
import pl.mulaczos.savings.domain.util.DateUtil
import spock.lang.Shared
import spock.lang.Unroll

import java.time.LocalDate

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static pl.mulaczos.savings.domain.constants.ApplicationConstants.NO_CATEGORY_KEY

@Transactional
class ExpenditureControllerSpec extends SharedExpendituresSpec {

    @Shared
    def locationHeaderName = "Location"

    def "Should add new expenditure with full details"() {
        given:
        def date = LocalDate.now().plusDays(3)
        def newExpenditure = new AddExpenditureDto(new BigDecimal(10), 1, date, 'comment', false, ['first', 'second'], 1)
        newExpenditure.name = 'wydatek'

        when:
        def location = persistNewExpenditure(newExpenditure)
                .response.getHeader(locationHeaderName)

        def response = getResponseAsMapFromHeaderLocationId(EXPENDITURES_BASE_PATH, location)

        then:
        response.name == 'wydatek'
        response.price == 10
        response.category.id == 1
        response.category.name == 'Jedzenie'
        response.category.position == 1
        getLocalDateFromString(response.eventDate) == date
        response.comment == 'comment'
        response.labels == ['first', 'second']
        !response.income
        response.account.id == 1
        response.account.name == 'DEFAULT'
        response.account.balance == 545.00
    }

    def "Should add new expenditure with only necessary data"() {
        given:
        def newExpenditure = getSimpleAddExpenditureDto()

        when:
        def location = persistNewExpenditure(newExpenditure).response.getHeader(locationHeaderName)
        def response = getResponseAsMapFromHeaderLocationId(EXPENDITURES_BASE_PATH, location)

        then:
        response.name == 'wydatek'
        response.price == 10
        response.category.name == NO_CATEGORY_KEY
        getLocalDateFromString(response.eventDate) == LocalDate.now()
        !response.comment
        !response.labels
    }

    def "Should throw an exception when dates are not specified"() {
        when:
        def res = mockMvc.perform(get("${EXPENDITURES_BASE_PATH}/groups"))

        then:
        res.andExpect(status().is(400))
    }

    @WithMockUser
    def "Should fetch three expenditures grouped by category for specified dates"() {
        given:
        prepareNewExpendituresWithSpecifiedDates()

        when:
        def res = mockMvc.perform(get("${EXPENDITURES_BASE_PATH}/groups")
                .param("since", DateUtil.startOfTheCurrentMonth.toString())
                .param("until", DateUtil.endOfCurrentMonth.toString()))
                .andExpect(status().is(200))
                .andReturn().response.contentAsString
        def response = jsonSlurper.parseText(res)

        then:
        def expenditures = response.get(NO_CATEGORY_KEY)
        expenditures.size() == 3
    }

    def "Should delete existing expenditure and update account balance"() {
        given:
        def defaultAccount = getDefaultAccountAsMap()
        def newExpenditure = getSimpleAddExpenditureDto(defaultAccount.id)
        def initialAccountBalance = defaultAccount.balance

        when:
        def location = persistNewExpenditure(newExpenditure).response.getHeader(locationHeaderName)
        def savedExpenditure = getResponseAsMapFromHeaderLocationId(EXPENDITURES_BASE_PATH, location)
        def afterExpenditureAddAccountBalance = getDefaultAccountAsMap().balance

        then:
        afterExpenditureAddAccountBalance == initialAccountBalance + savedExpenditure.price

        and:
        mockMvc.perform(delete("${EXPENDITURES_BASE_PATH}/$savedExpenditure.id"))
                .andExpect(status().is(204))

        then:
        getDefaultAccountAsMap().balance == initialAccountBalance
    }

    @Unroll
    def "Should edit existing expenditure and update account balance when first expenditure was income: #isNewExpenditureIncome and edited expenditure is income: #isEditedExpenditureIncome"() {
        given:
        def defaultAccount = getDefaultAccountAsMap()
        def newExpenditure = getSimpleAddExpenditureDto(defaultAccount.id, isNewExpenditureIncome)
        def initialAccountBalance = defaultAccount.balance

        when:
        def location = persistNewExpenditure(newExpenditure).response.getHeader(locationHeaderName)
        def savedExpenditure = getResponseAsMapFromHeaderLocationId(EXPENDITURES_BASE_PATH, location)
        def afterExpenditureAddAccountBalance = getDefaultAccountAsMap().balance

        then:
        afterExpenditureAddAccountBalance == initialAccountBalance + (savedExpenditure.price * modifier)
        def editedExpenditure = new AddExpenditureDto(id: savedExpenditure.id, name: 'name', price: 55,
                account: defaultAccount.id, income: isEditedExpenditureIncome)

        and:
        mockMvc.perform(put("${EXPENDITURES_BASE_PATH}/$savedExpenditure.id")
                .contentType(APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(editedExpenditure)))
                .andExpect(status().is(204))
                .andReturn()

        then:
        getDefaultAccountAsMap().balance == initialAccountBalance + resultBalance

        where:
        isNewExpenditureIncome | isEditedExpenditureIncome | resultBalance | modifier
        true                   | true                      | 55            | 1
        false                  | true                      | 55            | -1
        true                   | false                     | -55           | 1
        false                  | false                     | -55           | -1

    }
}
