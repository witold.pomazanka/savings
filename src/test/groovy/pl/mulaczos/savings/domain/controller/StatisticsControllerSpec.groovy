package pl.mulaczos.savings.domain.controller


import pl.mulaczos.savings.domain.SharedExpendituresSpec
import pl.mulaczos.savings.domain.constants.ApplicationConstants
import pl.mulaczos.savings.domain.util.DateUtil

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class StatisticsControllerSpec extends SharedExpendituresSpec {

    def BASE_PATH = "/api/statistics"

    def "Should return statistics for given period"() {
        given:
        prepareNewExpendituresWithSpecifiedDates()

        when:
        def res = mockMvc.perform(get("${BASE_PATH}")
                .param("since", DateUtil.startOfTheCurrentMonth.toString())
                .param("until", DateUtil.endOfCurrentMonth.toString()))
                .andExpect(status().is(200))
                .andReturn().response.contentAsString
        def response = jsonSlurper.parseText(res)

        then:
        response.totalExpenditureForAllCategories == 30
        def result = response.statistics.find { it.categoryName == ApplicationConstants.NO_CATEGORY_KEY }
        result.categoryId
        result.position
        result.sum == 30
        result.average == 0
    }

    def "Should return 400 on missing date parameters in request"() {
        when:
        def request = mockMvc.perform(get("${BASE_PATH}"))

        then:
        request.andExpect(status().is(400))
    }
}
