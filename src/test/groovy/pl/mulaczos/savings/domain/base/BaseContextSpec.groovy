package pl.mulaczos.savings.domain.base

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import groovy.json.JsonSlurper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.transaction.annotation.Transactional
import spock.lang.Specification

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.regex.Matcher
import java.util.regex.Pattern

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WithMockUser
@Transactional
@SpringBootTest
@AutoConfigureMockMvc
abstract class BaseContextSpec extends Specification {

    @Autowired
    MockMvc mockMvc

    def ACCOUNT_PATH = "/api/account"

    def jsonSlurper = new JsonSlurper()

    def getObjectMapper() {
        def mapper = new ObjectMapper()
        mapper.registerModule(new JavaTimeModule())
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    }

    def getLocalDateFromString(def date) {
        LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE)
    }

    def getResponseAsMapFromHeaderLocationId(def path, def location) {
        Matcher matcher = Pattern.compile("\\d+").matcher(location)
        matcher.find()
        def id = Long.valueOf(matcher.group());
        def getRequestResponse = mockMvc.perform(get("${path}/$id"))
                .andReturn().response.contentAsString
        jsonSlurper.parseText(getRequestResponse)
    }

    def getDefaultAccountAsMap() {
        jsonSlurper.parseText(mockMvc.perform(get("${ACCOUNT_PATH}/default"))
                .andExpect(status().is(200))
                .andReturn().response.contentAsString)
    }
}
