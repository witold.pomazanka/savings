package pl.mulaczos.savings.domain.service


import pl.mulaczos.savings.domain.model.Category
import pl.mulaczos.savings.domain.repository.CategoryRepository
import pl.mulaczos.savings.domain.repository.ExpenditureRepository
import pl.mulaczos.savings.domain.util.DateUtil
import spock.lang.Specification

class StatisticsServiceSpec extends Specification {

    def expenditureRepository = Mock(ExpenditureRepository)
    def categoryRepository = Mock(CategoryRepository)
    def subject = new StatisticsService(expenditureRepository, categoryRepository)

    def "Should return statistics for appropriate period"() {
        given:
        def since = DateUtil.getStartOfTheCurrentMonth()
        def until = DateUtil.getEndOfCurrentMonth()

        when:

        subject.findCategoriesWithLabelsAndTheirSumsForGivenPeriod(since, until)

        then:
        1 * categoryRepository.findAllByOrderByPositionAsc() >> [new Category(id: 1l, position: 1, name: 'name'), new Category(id: 1l, position: 2, name: 'name2'), new Category(id: 1l, position: 3, name: 'name3')]
        3 * expenditureRepository.sumByCategorySinceUntil(_ as Long, since, until) >> 0.0
    }
}
