package pl.mulaczos.savings.domain.service

import pl.mulaczos.savings.configuration.mapper.ExpenditureMapper
import pl.mulaczos.savings.domain.dto.AccountDto
import pl.mulaczos.savings.domain.dto.AddExpenditureDto
import pl.mulaczos.savings.domain.model.Account
import pl.mulaczos.savings.domain.model.Category
import pl.mulaczos.savings.domain.model.Expenditure
import pl.mulaczos.savings.domain.repository.CategoryRepository
import pl.mulaczos.savings.domain.repository.ExpenditureRepository
import pl.mulaczos.savings.domain.util.DateUtil
import spock.lang.Specification

class ExpenditureServiceSpec extends Specification {

    def mapper = Mock(ExpenditureMapper)
    def expenditureRepository = Mock(ExpenditureRepository)
    def categoryRepository = Mock(CategoryRepository)
    def accountService = Mock(AccountService)
    def subject = new ExpenditureService(expenditureRepository, categoryRepository, accountService, mapper)

    def "Should add expenditure and invoke account balance update"() {
        given:
        def id = 7
        def accountId = 10
        def isIncome = true
        def amount = new BigDecimal(100)
        def dto = new AddExpenditureDto(id: id, account: accountId, income: isIncome, price: amount)
        def expenditure = new Expenditure(id: id, account: new Account(id: accountId, name: 'name', balance: 555))

        when:
        def expectedId = subject.add(dto)

        then:
        1 * mapper.addExpenditureDtoToExpenditure(dto) >> expenditure
        1 * accountService.updateBalance(accountId, amount, isIncome) >> null
        1 * expenditureRepository.save(expenditure) >> expenditure
        expectedId == id
    }

    def "Should delete expenditure and invoke account balance update"() {
        given:
        def id = 7
        def accountId = 10
        def isIncome = false
        def amount = new BigDecimal(100)
        def expenditure = new Expenditure(id: id,
                account: new Account(id: accountId, name: 'name', balance: 555),
                price: amount,
                income: isIncome)

        when:
        subject.delete(id)

        then:
        1 * expenditureRepository.getOne(id) >> expenditure
        1 * accountService.updateBalance(accountId, amount, !isIncome) >> null
        1 * expenditureRepository.delete(expenditure)
    }

    def "Should return expenditures grouped by category if dates are month defaults"() {
        given:
        def since = DateUtil.getStartOfTheCurrentMonth()
        def until = DateUtil.getEndOfCurrentMonth()
        def accountId = 55

        when:
        subject.findAllGroupByCategory(since, until)

        then:
        1 * categoryRepository.findAllByOrderByPositionAsc() >> [new Category(), new Category(), new Category()]
        1 * accountService.getDefaultAccount() >> new AccountDto(id: accountId)
        3 * expenditureRepository.findAllByCategoryAndAccountIdAndEventDateGreaterThanEqualAndEventDateLessThanEqualOrderByEventDateDesc(_ as Category, accountId, since, until) >> [new Expenditure()]
        3 * mapper.entityToDto([new Expenditure()]) >> []
    }
}
