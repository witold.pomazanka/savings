package pl.mulaczos.savings.domain.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import pl.mulaczos.savings.domain.base.BaseContextSpec
import pl.mulaczos.savings.domain.model.Expenditure
import pl.mulaczos.savings.domain.repository.AccountRepository
import pl.mulaczos.savings.domain.repository.CategoryRepository
import pl.mulaczos.savings.domain.repository.ExpenditureRepository
import pl.mulaczos.savings.domain.util.DateUtil

import java.time.LocalDate

@Transactional
class ExpenditureRepositoryIntegrationSpec extends BaseContextSpec {

    @Autowired
    private ExpenditureRepository repository

    @Autowired
    private CategoryRepository categoryRepository

    @Autowired
    private AccountRepository accountRepository

    def "Should find three expenditures"() {
        given:
        def atStartOfTheMonth = DateUtil.startOfTheCurrentMonth
        def inTheMiddleOfTheMonth = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), 15)
        def atTheEndOfMonth = DateUtil.endOfCurrentMonth
        def category = categoryRepository.getOne(1l)
        def accountId = 1l
        def account = accountRepository.getOne(accountId)

        when:
        repository.save(new Expenditure(name: 'name1', category: category, price: 10, eventDate: atStartOfTheMonth, account: account))
        repository.save(new Expenditure(name: 'name2', category: category, price: 20, eventDate: inTheMiddleOfTheMonth, account: account))
        repository.save(new Expenditure(name: 'name3', category: category, price: 30, eventDate: atTheEndOfMonth, account: account))

        then:
        def result = repository.findAllByCategoryAndAccountIdAndEventDateGreaterThanEqualAndEventDateLessThanEqualOrderByEventDateDesc(category, accountId, DateUtil.startOfTheCurrentMonth, DateUtil.endOfCurrentMonth)
        println repository.findAll().size()
        result.size() == 3

    }
}
