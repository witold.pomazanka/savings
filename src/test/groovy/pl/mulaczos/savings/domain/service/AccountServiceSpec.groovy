package pl.mulaczos.savings.domain.service

import pl.mulaczos.savings.configuration.mapper.AccountMapper
import pl.mulaczos.savings.domain.dto.AccountDto
import pl.mulaczos.savings.domain.model.Account
import pl.mulaczos.savings.domain.repository.AccountRepository
import spock.lang.Specification

class AccountServiceSpec extends Specification {

    def mapper = Mock(AccountMapper)
    def repository = Mock(AccountRepository)
    def subject = new AccountService(repository, mapper)

    def "Should get default account"() {
        given:
        def account = new Account(id: 1, name: 'name', balance: 100)
        def accountDto = new AccountDto(id: 1, name: 'name', balance: 100)

        when:
        subject.getDefaultAccount()

        then:
        1 * repository.findOneByDefaultAccountTrue() >> account
        1 * mapper.entityToDto(account) >> accountDto
    }
}
