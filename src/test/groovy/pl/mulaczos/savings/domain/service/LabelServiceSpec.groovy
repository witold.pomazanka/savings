package pl.mulaczos.savings.domain.service


import pl.mulaczos.savings.configuration.mapper.LabelMapper
import pl.mulaczos.savings.domain.dto.LabelDto
import pl.mulaczos.savings.domain.model.Label
import pl.mulaczos.savings.domain.repository.LabelRepository
import spock.lang.Shared
import spock.lang.Specification

class LabelServiceSpec extends Specification {

    def mapper = Mock(LabelMapper)
    def labelRepository = Mock(LabelRepository)
    def subject = new LabelService(labelRepository, mapper)

    @Shared
    def labelDto = new LabelDto(name: 'newName')

    @Shared
    def label = new Label(name: 'newName')

    def "Should find all labels"() {
        given:
        def labels = [new Label(id: 1, name: 'name'), new Label(id: 2, name: 'name2')]
        def labelDtos = [new Label(id: 1, name: 'name'), new Label(id: 2, name: 'name2')]

        when:
        def response = subject.findAll()

        then:
        1 * labelRepository.findAllByOrderByNameDesc() >> labels
        1 * mapper.entityToDto(labels) >> labelDtos
        response.size() == 2
    }

    def "Should add new label"() {
        when:
        subject.add(labelDto)

        then:
        1 * mapper.dtoToEntity(labelDto) >> label
        1 * labelRepository.save(label)
    }

    def "Should get existing label"() {
        given:
        def id = 1

        when:
        subject.getOne(id)

        then:
        1 * labelRepository.getOne(id) >> label
        1 * mapper.entityToDto(label)
    }

    def "Should update existing label"() {
        when:
        subject.add(labelDto)

        then:
        1 * mapper.dtoToEntity(labelDto) >> label
        1 * labelRepository.save(label)
    }

    def "Should delete existing label"() {
        given:
        def id = 1

        when:
        subject.delete(id)

        then:
        1 * labelRepository.deleteById(id)
    }
}
