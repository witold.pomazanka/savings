package pl.mulaczos.savings.domain.service

import org.springframework.beans.factory.annotation.Autowired
import pl.mulaczos.savings.domain.base.BaseContextSpec
import pl.mulaczos.savings.domain.dto.AccountDto
import pl.mulaczos.savings.domain.dto.AddAccountDto

class AccountServiceIntegrationSpec extends BaseContextSpec {

    @Autowired
    private AccountService accountService

    def "Should add new account"() {
        given:
        def newAccountName = 'newAccount'
        def newAccountBalance = new BigDecimal(10)
        def addAccountDto = new AddAccountDto(newAccountBalance, newAccountName)

        when:
        def newlyCreatedAccountId = accountService.add(addAccountDto)
        def newlyCreatedAccount = accountService.getOne(newlyCreatedAccountId)

        then:
        newlyCreatedAccount.id == newlyCreatedAccountId
        newlyCreatedAccount.name == newAccountName
        newlyCreatedAccount.balance == newAccountBalance
        !newlyCreatedAccount.defaultAccount
    }

    def "Should change only name of account not balance"() {
        given:
        def existingAccountId = 1
        def newAccountName = 'newName'
        def newAccountBalance = new BigDecimal(9999)
        def accountDto = new AccountDto(id: existingAccountId, balance: newAccountBalance, name: newAccountName)
        def existingAccount = accountService.getOne(existingAccountId)
        def existingAccountBalance = existingAccount.balance
        def existingAccountName = existingAccount.name

        when:
        accountService.update(accountDto)
        def accountAfterUpdate = accountService.getOne(existingAccountId)

        then:
        existingAccountName != newAccountName
        accountAfterUpdate.name == newAccountName
        accountAfterUpdate.balance == existingAccountBalance
        existingAccountBalance != newAccountBalance

    }

    def "Should subtract from account"() {
        given:
        def id = 1

        when:
        def account = accountService.getOne(id)

        then:
        account.balance == 555

        and:
        accountService.updateBalance(id, new BigDecimal(55), false)
        def accountAfterUpdate = accountService.getOne(id)

        then:
        accountAfterUpdate.balance == 500
    }

    def "Should add to account"() {
        given:
        def id = 1

        when:
        def account = accountService.getOne(id)

        then:
        account.balance == 555

        and:
        accountService.updateBalance(id, new BigDecimal(45), true)
        def accountAfterUpdate = accountService.getOne(id)

        then:
        accountAfterUpdate.balance == 600
    }

    def "Should set default account"() {
        given:
        def defaultAccountId = 1
        def nonDefaultAccountId = 3

        when:
        def defaultAccount = accountService.getOne(defaultAccountId)
        def nonDefaultAccount = accountService.getOne(nonDefaultAccountId)

        then:
        defaultAccount.defaultAccount
        !nonDefaultAccount.defaultAccount

        and:
        accountService.makeDefault(nonDefaultAccountId)

        then:
        def accountAfterUpdateShouldNowBeDefault = accountService.getOne(nonDefaultAccountId)
        accountAfterUpdateShouldNowBeDefault.defaultAccount
        def shouldNotBeDefaultAnymore = accountService.getOne(defaultAccountId)
        !shouldNotBeDefaultAnymore.defaultAccount
    }

    def "Should throw an exception when account with given id does not exist"() {
        given:
        def id = 999

        when:
        accountService.makeDefault(id)

        then:
        thrown(IllegalArgumentException)
    }
}
