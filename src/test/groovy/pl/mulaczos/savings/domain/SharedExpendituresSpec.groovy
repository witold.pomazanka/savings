package pl.mulaczos.savings.domain

import pl.mulaczos.savings.domain.base.BaseContextSpec
import pl.mulaczos.savings.domain.dto.AddExpenditureDto
import pl.mulaczos.savings.domain.util.DateUtil
import spock.lang.Shared

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class SharedExpendituresSpec extends BaseContextSpec {

    @Shared
    def EXPENDITURES_BASE_PATH = '/api/expenditure'

    def prepareNewExpendituresWithSpecifiedDates() {
        persistNewExpenditure(getSimpleAddExpenditureDto())
        def atTheStartOfThisMonth = getSimpleAddExpenditureDto()
        atTheStartOfThisMonth.setEventDate(DateUtil.startOfTheCurrentMonth)
        persistNewExpenditure(atTheStartOfThisMonth)
        def atTheEndOfThisMonth = getSimpleAddExpenditureDto()
        atTheEndOfThisMonth.setEventDate(DateUtil.endOfCurrentMonth)
        persistNewExpenditure(atTheEndOfThisMonth)
    }

    def persistNewExpenditure(AddExpenditureDto data) {
        mockMvc.perform(post(EXPENDITURES_BASE_PATH)
                .contentType(APPLICATION_JSON)
                .content(getObjectMapper().writeValueAsString(data)))
                .andExpect(status().is(201))
                .andReturn()
    }

    def getSimpleAddExpenditureDto() {
        this.getSimpleAddExpenditureDto(1, true)
    }

    def getSimpleAddExpenditureDto(def accountId) {
        this.getSimpleAddExpenditureDto(accountId, true)
    }

    def getSimpleAddExpenditureDto(boolean isIncome) {
        this.getSimpleAddExpenditureDto(1, isIncome)
    }

    def getSimpleAddExpenditureDto(def accountId, boolean isIncome) {
        new AddExpenditureDto(
                name: 'wydatek',
                price: 10,
                account: accountId,
                income: isIncome
        )
    }
}
