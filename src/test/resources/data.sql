INSERT INTO USERS (ID, USERNAME, PASSWORD, ENABLED, NAME, LASTNAME, EMAIL)
VALUES (1, 'user', 'password', TRUE, 'name', 'lastname', 'email');

INSERT INTO CATEGORIES(ID, NAME, POSITION)
VALUES (1, 'Jedzenie', 1),
       (2, 'Jedzenie poza domem', 2),
       (3, 'Alkohol', 3),
       (4, 'Alkohol poza domem', 4),
       (5, 'Dla siebie', 5),
       (6, 'Dla kogoś', 6),
       (7, 'Mazda <3', 7),
       (8, 'Transport', 8),
       (9, 'Dom', 9),
       (10, 'Wypad', 10),
       (11, 'Ogólne', 11),
       (12, 'Brak', 12);

INSERT INTO CATEGORIES(ID, NAME, POSITION)
VALUES (13, 'Odzież', (SELECT MAX(POSITION) FROM CATEGORIES) + 1);
INSERT INTO CATEGORIES(ID, NAME, POSITION)
VALUES (14, 'Gadżety', (SELECT MAX(POSITION) FROM CATEGORIES) + 1);

INSERT INTO CATEGORIES(ID, NAME, POSITION)
VALUES (20, 'NO CATEGORY', (SELECT MAX(POSITION) FROM CATEGORIES) + 1);

INSERT INTO ACCOUNTS(ID, NAME, BALANCE, IS_DEFAULT)
VALUES (1, 'DEFAULT', 555, TRUE),
       (2, 'SECOND', 222, FALSE),
       (3, 'THIRD', 333, FALSE),
       (4, 'FOURTH', 444, FALSE);
