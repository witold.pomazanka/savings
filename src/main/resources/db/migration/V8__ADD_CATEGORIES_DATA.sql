INSERT INTO CATEGORIES(NAME, POSITION)
VALUES ('Jedzenie', 1),
       ('Jedzenie poza domem', 2),
       ('Alkohol', 3),
       ('Alkohol poza domem', 4),
       ('Dla siebie', 5),
       ('Dla kogoś', 6),
       ('Mazda <3', 7),
       ('Transport', 8),
       ('Dom', 9),
       ('Wypad', 10),
       ('Ogólne', 11),
       ('Brak', 12);
