CREATE TABLE EXPENDITURE_LABELS
(
  ID   BIGSERIAL    NOT NULL PRIMARY KEY,
  NAME VARCHAR(255) NOT NULL,
  EXPENDITURE_ID BIGINT REFERENCES EXPENDITURES
);

COMMIT;