package pl.mulaczos.savings.domain.repository

import org.springframework.stereotype.Repository
import pl.mulaczos.savings.domain.model.Settings

@Repository
interface SettingsRepository : BaseRepository<Settings>
