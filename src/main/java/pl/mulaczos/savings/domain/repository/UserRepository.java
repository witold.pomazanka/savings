package pl.mulaczos.savings.domain.repository;

import org.springframework.stereotype.Repository;
import pl.mulaczos.savings.configuration.security.model.User;

@Repository
public interface UserRepository extends BaseRepository<User> {
    User findByUsername(String name);
}
