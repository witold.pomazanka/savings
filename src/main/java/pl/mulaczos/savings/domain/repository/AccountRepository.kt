package pl.mulaczos.savings.domain.repository

import org.springframework.stereotype.Repository
import pl.mulaczos.savings.domain.model.Account

@Repository
interface AccountRepository : BaseRepository<Account> {
    fun findOneByDefaultAccountTrue(): Account
}
