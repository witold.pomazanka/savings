package pl.mulaczos.savings.domain.repository;

import java.util.List;
import org.springframework.stereotype.Repository;
import pl.mulaczos.savings.domain.model.Category;

@Repository
public interface CategoryRepository extends BaseRepository<Category> {
    List<Category> findAllByOrderByPositionAsc();

    Category findByName(String name);
}
