package pl.mulaczos.savings.domain.repository

import org.springframework.stereotype.Repository
import pl.mulaczos.savings.domain.model.Label

@Repository
interface LabelRepository : BaseRepository<Label> {
    fun findAllByOrderByNameDesc(): List<Label>
    fun existsByName(name: String): Boolean
}
