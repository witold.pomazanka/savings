package pl.mulaczos.savings.domain.repository;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.mulaczos.savings.domain.model.Category;
import pl.mulaczos.savings.domain.model.Expenditure;

@Repository
public interface ExpenditureRepository extends BaseRepository<Expenditure> {

    List<Expenditure> findAllByOrderByEventDateAsc();

    List<Expenditure> findAllByCategoryAndAccountIdAndEventDateGreaterThanEqualAndEventDateLessThanEqualOrderByEventDateDesc(
        Category category, Long accountId, LocalDate since, LocalDate until);

    @Query("SELECT SUM(e.price) FROM Expenditure e WHERE e.category.id = :categoryId AND e.eventDate BETWEEN :since AND :until")
    Double sumByCategorySinceUntil(Long categoryId, LocalDate since, LocalDate until);
}
