package pl.mulaczos.savings.domain.util;

import java.time.LocalDate;
import org.springframework.stereotype.Component;

@Component
public class DateUtil {

    public static LocalDate getStartOfTheCurrentMonth() {
        return LocalDate.now().withDayOfMonth(1);
    }

    public static LocalDate getEndOfCurrentMonth() {
        return LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth());
    }
}
