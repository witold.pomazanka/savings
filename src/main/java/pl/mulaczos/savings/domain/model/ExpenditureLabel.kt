package pl.mulaczos.savings.domain.model

import javax.persistence.Entity
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "EXPENDITURE_LABELS")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "EXPENDITURE_LABELS_ID_SEQ", allocationSize = 1)
class ExpenditureLabel : BaseNamedEntity()