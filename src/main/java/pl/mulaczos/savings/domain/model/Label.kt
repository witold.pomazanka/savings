package pl.mulaczos.savings.domain.model

import javax.persistence.Entity
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "LABELS")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "LABELS_ID_SEQ", allocationSize = 1)
class Label : BaseNamedEntity()
