package pl.mulaczos.savings.domain.model

import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "ACCOUNTS")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "ACCOUNTS_ID_SEQ", allocationSize = 1)
data class Account(var balance: BigDecimal?,
                   @Column(name = "IS_DEFAULT") var defaultAccount: Boolean?) : BaseNamedEntity() {
    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null, null)
}
