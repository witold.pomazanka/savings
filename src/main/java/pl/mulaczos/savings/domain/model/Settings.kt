package pl.mulaczos.savings.domain.model

import javax.persistence.Entity
import javax.persistence.SequenceGenerator

@Entity
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "SETTINGS_ID_SEQ", allocationSize = 1)
class Settings : BaseEntity()
