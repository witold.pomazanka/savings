package pl.mulaczos.savings.domain.model

import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.*

@Entity
@Table(name = "EXPENDITURES")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "EXPENDITURES_ID_SEQ", allocationSize = 1)
data class Expenditure(var price: BigDecimal?,
                       @ManyToOne var category: Category?,
                       var comment: String?,
                       var eventDate: LocalDate?,
                       var income: Boolean?,
                       @ManyToOne var account: Account?,
                       @OneToMany(cascade = [CascadeType.PERSIST, CascadeType.MERGE])
                       @JoinColumn(name = "EXPENDITURE_ID") var labels: List<ExpenditureLabel>?) : BaseNamedEntity() {
    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null, null, null, null, null, null, null)
}
