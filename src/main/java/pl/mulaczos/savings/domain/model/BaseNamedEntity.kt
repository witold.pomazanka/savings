package pl.mulaczos.savings.domain.model

import javax.persistence.MappedSuperclass

@MappedSuperclass
open class BaseNamedEntity : BaseEntity() {
    var name: String? = null
}