package pl.mulaczos.savings.domain.model

import javax.persistence.Entity
import javax.persistence.SequenceGenerator
import javax.persistence.Table

@Entity
@Table(name = "CATEGORIES")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "CATEGORIES_ID_SEQ", allocationSize = 1)
data class Category(var position: Int?) : BaseNamedEntity() {
    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null)
}