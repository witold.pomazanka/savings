package pl.mulaczos.savings.domain.dto

open class BaseNamedDto : BaseDto() {
    var name: String? = null
}