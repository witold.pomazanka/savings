package pl.mulaczos.savings.domain.dto

data class StatisticsDto(val categoryId: Long,
                         val position: Int,
                         val categoryName: String,
                         val sum: Double,
                         val average: Double)