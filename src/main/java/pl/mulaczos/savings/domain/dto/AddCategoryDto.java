package pl.mulaczos.savings.domain.dto;

import lombok.Data;

@Data
public class AddCategoryDto {

    private String name;
    private Integer position;
}
