package pl.mulaczos.savings.domain.dto

import java.math.BigDecimal

data class AddAccountDto(var balance: BigDecimal?,
                         var name: String?) {
    constructor() : this(null, null)
}
