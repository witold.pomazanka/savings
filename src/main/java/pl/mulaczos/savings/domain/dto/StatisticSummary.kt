package pl.mulaczos.savings.domain.dto

data class StatisticSummary(val totalExpenditureForAllCategories: Double,
                            val statistics: List<StatisticsDto>)