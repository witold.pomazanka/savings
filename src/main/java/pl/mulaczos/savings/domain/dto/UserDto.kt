package pl.mulaczos.savings.domain.dto

data class UserDto(var username: String?, var lastname: String?, var email: String?, var enabled: Boolean?, var authorities: List<String?>?) {
    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null, null, null, null, null)
}