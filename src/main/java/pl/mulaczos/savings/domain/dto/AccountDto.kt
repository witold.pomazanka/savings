package pl.mulaczos.savings.domain.dto

import java.math.BigDecimal

data class AccountDto(var balance: BigDecimal?,
                      var defaultAccount: Boolean?) : BaseNamedDto() {
    constructor() : this(null, null)
}
