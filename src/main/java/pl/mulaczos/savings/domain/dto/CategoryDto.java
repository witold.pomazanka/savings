package pl.mulaczos.savings.domain.dto;

import lombok.Data;

@Data
public class CategoryDto extends BaseNamedDto {

    private Integer position;
}
