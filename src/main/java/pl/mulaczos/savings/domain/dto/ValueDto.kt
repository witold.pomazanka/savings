package pl.mulaczos.savings.domain.dto

import java.math.BigDecimal

data class ValueDto(val value: BigDecimal)
