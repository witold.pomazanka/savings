package pl.mulaczos.savings.domain.dto

import java.math.BigDecimal
import java.time.LocalDate

data class AddExpenditureDto(var price: BigDecimal?,
                             var category: Long?,
                             var eventDate: LocalDate?,
                             var comment: String?,
                             var income: Boolean?,
                             var labels: List<String>?,
                             var account: Long?) : BaseNamedDto() {
    constructor() : this(null, null, null, null, null, null, null)
}
