package pl.mulaczos.savings.domain.dto

import com.fasterxml.jackson.annotation.JsonInclude
import java.math.BigDecimal
import java.time.LocalDate

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ExpenditureDto(var price: BigDecimal?,
                          var category: CategoryDto?,
                          var eventDate: LocalDate?,
                          var comment: String?,
                          var income: Boolean?,
                          var labels: List<String>?,
                          var account: AccountDto?) : BaseNamedDto() {

    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null, null, null, null, null, null, null)
}
