package pl.mulaczos.savings.domain.constants;

public class ApplicationConstants {
    public static final String API = "api/";
    public static final String NO_CATEGORY_KEY = "NO CATEGORY";
}