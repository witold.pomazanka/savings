package pl.mulaczos.savings.domain.controller;

import static pl.mulaczos.savings.domain.constants.ApplicationConstants.API;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.mulaczos.savings.domain.dto.AddExpenditureDto;
import pl.mulaczos.savings.domain.dto.ExpenditureDto;
import pl.mulaczos.savings.domain.service.ExpenditureService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(API + "/expenditure")
@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_MODERATOR','ROLE_ADMIN')")
public class ExpenditureController {

    private final ExpenditureService service;

    @GetMapping
    public List<ExpenditureDto> findAll() {
        return this.service.findAll();
    }

    @GetMapping("/groups")
    public Map<String, List<ExpenditureDto>> findAllGroupByCategory(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate since,
                                                                    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam LocalDate until) {
        return this.service.findAllGroupByCategory(since, until);
    }

    @PostMapping
    public ResponseEntity<Long> add(@RequestBody AddExpenditureDto dto) {
        var id = this.service.add(dto);
        URI location = URI.create(String.format("%sexpenditure/%s", API, id));
        return ResponseEntity.created(location).build();
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> update(@PathVariable Long id, @RequestBody AddExpenditureDto dto) {
        if (id != dto.getId()) {
            throw new IllegalArgumentException("Id from path differs with id from payload");
        }
        this.service.update(dto);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}")
    public ExpenditureDto getOne(@PathVariable Long id) {
        return this.service.getOne(id);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        this.service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
