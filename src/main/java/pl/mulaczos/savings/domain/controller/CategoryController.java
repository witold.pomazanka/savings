package pl.mulaczos.savings.domain.controller;

import static pl.mulaczos.savings.domain.constants.ApplicationConstants.API;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mulaczos.savings.domain.dto.AddCategoryDto;
import pl.mulaczos.savings.domain.dto.CategoryDto;
import pl.mulaczos.savings.domain.service.CategoryService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(API + "/category")
@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_MODERATOR','ROLE_ADMIN')")
public class CategoryController {

    private final CategoryService service;

    @GetMapping
    public List<CategoryDto> findAll() {
        return service.findAll();
    }

    @PostMapping
    public CategoryDto add(@RequestBody AddCategoryDto dto) {
        return service.add(dto);
    }

    @PutMapping
    public void update(@RequestBody List<CategoryDto> dto) {
        service.update(dto);
    }
}
