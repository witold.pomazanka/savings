package pl.mulaczos.savings.domain.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.mulaczos.savings.domain.constants.ApplicationConstants.API
import pl.mulaczos.savings.domain.dto.StatisticSummary
import pl.mulaczos.savings.domain.service.StatisticsService
import java.time.LocalDate

@RestController
@RequestMapping("${API}/statistics")
@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_MODERATOR','ROLE_ADMIN')")
open class StatisticsController(@Autowired var service: StatisticsService) {

    @GetMapping
    open fun findCategoriesWithLabelsAndTheirSumsForGivenPeriod(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam since: LocalDate,
                                                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam until: LocalDate): ResponseEntity<StatisticSummary> {
        return ResponseEntity.ok(this.service.findCategoriesWithLabelsAndTheirSumsForGivenPeriod(since, until))
    }
}