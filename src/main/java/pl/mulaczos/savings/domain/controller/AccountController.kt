package pl.mulaczos.savings.domain.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import pl.mulaczos.savings.domain.constants.ApplicationConstants
import pl.mulaczos.savings.domain.dto.AccountDto
import pl.mulaczos.savings.domain.dto.AddAccountDto
import pl.mulaczos.savings.domain.service.AccountService
import java.net.URI

@RestController
@RequestMapping("${ApplicationConstants.API}/account")
@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_MODERATOR','ROLE_ADMIN')")
open class AccountController(@Autowired var service: AccountService) {

    @GetMapping("default")
    open fun getDefaultAccount(): ResponseEntity<AccountDto> {
        return ResponseEntity.ok(this.service.getDefaultAccount())
    }

    @PatchMapping("default/{id}")
    open fun makeDefault(@PathVariable id: Long): ResponseEntity<Void> {
        this.service.makeDefault(id)
        return ResponseEntity.noContent().build()
    }

    @GetMapping("{id}")
    open fun getOne(@PathVariable id: Long): ResponseEntity<AccountDto> {
        return ResponseEntity.ok(this.service.getOne(id))
    }

    @GetMapping
    open fun findAll(): ResponseEntity<List<AccountDto>> {
        return ResponseEntity.ok(this.service.findAll())
    }

    @PostMapping
    open fun add(@RequestBody dto: AddAccountDto): ResponseEntity<Long?>? {
        val id: Long = this.service.add(dto)
        val location = URI.create(String.format("%saccount/%s", ApplicationConstants.API, id))
        return ResponseEntity.created(location).build()
    }

    @PatchMapping("{id}")
    open fun update(@PathVariable id: Long, @RequestBody dto: AccountDto): ResponseEntity<Void> {
        if (dto.id != id) {
            throw IllegalArgumentException()
        }
        this.service.update(dto)
        return ResponseEntity.noContent().build()
    }

    @DeleteMapping("{id}")
    open fun delete(@PathVariable id: Long): ResponseEntity<Void> {
        this.service.delete(id)
        return ResponseEntity.noContent().build()
    }
}
