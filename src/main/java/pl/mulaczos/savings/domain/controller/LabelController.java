package pl.mulaczos.savings.domain.controller;

import static pl.mulaczos.savings.domain.constants.ApplicationConstants.API;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mulaczos.savings.domain.dto.LabelDto;
import pl.mulaczos.savings.domain.service.LabelService;

@RestController
@RequiredArgsConstructor
@RequestMapping(API + "/label")
@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_MODERATOR','ROLE_ADMIN')")
public class LabelController {

    private final LabelService labelService;

    @GetMapping
    public List<LabelDto> findAll() {
        return labelService.findAll();
    }

    @GetMapping("{id}")
    public LabelDto get(@PathVariable Long id) {
        return labelService.getOne(id);
    }

    @PostMapping
    public void add(@RequestBody LabelDto labelDto) {
        labelService.add(labelDto);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        labelService.delete(id);
    }

    @PutMapping("{id}")
    public void edit(@PathVariable Long id, @RequestBody LabelDto labelDto) {
        if (labelDto.getId() != id) {
            throw new IllegalArgumentException();
        }
        labelService.add(labelDto);
    }
}
