package pl.mulaczos.savings.domain.controller;

import java.security.Principal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.mulaczos.savings.domain.constants.ApplicationConstants;
import pl.mulaczos.savings.domain.dto.UserDto;
import pl.mulaczos.savings.domain.service.UserService;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(ApplicationConstants.API + "user")
public class UserController {

    private final UserService userService;

    @GetMapping("get-authentication")
    public UserDto getAuthentication(Principal principal) {
        return userService.findByUsername(principal);
    }
}
