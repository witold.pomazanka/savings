package pl.mulaczos.savings.domain.service

import org.springframework.stereotype.Service
import pl.mulaczos.savings.domain.dto.StatisticSummary
import pl.mulaczos.savings.domain.dto.StatisticsDto
import pl.mulaczos.savings.domain.repository.CategoryRepository
import pl.mulaczos.savings.domain.repository.ExpenditureRepository
import java.time.LocalDate
import java.util.Objects.nonNull

@Service
open class StatisticsService(private val expenditureRepository: ExpenditureRepository,
                             private val categoryRepository: CategoryRepository) {

    fun findCategoriesWithLabelsAndTheirSumsForGivenPeriod(since: LocalDate, until: LocalDate): StatisticSummary {
        val categories = categoryRepository.findAllByOrderByPositionAsc()
        val data = arrayListOf<StatisticsDto>()
        var total = 0.0
        categories.forEach {
            var sum = expenditureRepository.sumByCategorySinceUntil(it.id, since, until)
            sum = if (nonNull(sum)) sum else 0.0
            val avg = 0.0 // TODO implement
            total += sum
            data.add(StatisticsDto(it.id, it.position!!, it.name!!, sum, avg))
        }
        return StatisticSummary(total, data)
    }
}