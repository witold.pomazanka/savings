package pl.mulaczos.savings.domain.service;

import java.util.List;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mulaczos.savings.configuration.mapper.LabelMapper;
import pl.mulaczos.savings.domain.dto.LabelDto;
import pl.mulaczos.savings.domain.repository.LabelRepository;

@Service
@Transactional
@RequiredArgsConstructor
public class LabelService {

    private final LabelRepository labelRepository;
    private final LabelMapper mapper;

    public List<LabelDto> findAll() {
        return mapper.entityToDto(labelRepository.findAllByOrderByNameDesc());
    }

    public void add(LabelDto dto) {
        labelRepository.save(mapper.dtoToEntity(dto));
    }

    public LabelDto getOne(Long id) {
        return this.mapper.entityToDto(this.labelRepository.getOne(id));
    }

    public void delete(Long id) {
        this.labelRepository.deleteById(id);
    }
}
