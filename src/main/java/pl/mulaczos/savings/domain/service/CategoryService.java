package pl.mulaczos.savings.domain.service;

import java.util.List;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mulaczos.savings.configuration.mapper.CategoryMapper;
import pl.mulaczos.savings.domain.dto.AddCategoryDto;
import pl.mulaczos.savings.domain.dto.CategoryDto;
import pl.mulaczos.savings.domain.repository.CategoryRepository;

@Service
@Transactional
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository repository;
    private final CategoryMapper mapper;

    public List<CategoryDto> findAll() {
        return mapper.entityToDto(repository.findAllByOrderByPositionAsc());
    }

    public CategoryDto add(AddCategoryDto dto) {
        return mapper.entityToDto(repository.save(mapper.addCategoryToCategory(dto)));
    }

    public void update(List<CategoryDto> dto) {
        repository.saveAll(mapper.dtoToEntity(dto));
    }
}
