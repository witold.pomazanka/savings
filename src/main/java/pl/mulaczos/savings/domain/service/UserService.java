package pl.mulaczos.savings.domain.service;

import static java.util.Objects.nonNull;

import java.security.Principal;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mulaczos.savings.configuration.mapper.UserMapper;
import pl.mulaczos.savings.domain.dto.UserDto;
import pl.mulaczos.savings.domain.repository.UserRepository;

@Service
@AllArgsConstructor
public class UserService {

    private UserMapper mapper;
    private UserRepository userRepository;

    public UserDto findByUsername(Principal principal) {
        if (nonNull(principal)) {
            return mapper.entityToDto(userRepository.findByUsername(principal.getName()));
        } else {
            return null;
        }
    }
}
