package pl.mulaczos.savings.domain.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.mulaczos.savings.configuration.mapper.AccountMapper
import pl.mulaczos.savings.domain.dto.AccountDto
import pl.mulaczos.savings.domain.dto.AddAccountDto
import pl.mulaczos.savings.domain.repository.AccountRepository
import java.math.BigDecimal

@Service
@Transactional
open class AccountService(private val repository: AccountRepository,
                          private val mapper: AccountMapper) {

    open fun getDefaultAccount(): AccountDto {
        return this.mapper.entityToDto(this.repository.findOneByDefaultAccountTrue())
    }

    open fun findAll(): List<AccountDto> {
        return this.mapper.entityToDto(this.repository.findAll())!!
    }

    open fun getOne(id: Long): AccountDto {
        return this.mapper.entityToDto(this.repository.getOne(id))
    }

    open fun add(dto: AddAccountDto): Long {
        return this.repository.save(this.mapper.addAccountDtoToAccount(dto)).id
    }

    open fun update(dto: AccountDto) {
        val accountToUpdate = this.repository.getOne(dto.id)
        accountToUpdate.name = dto.name
    }

    open fun updateBalance(accountId: Long, amount: BigDecimal, isIncome: Boolean) {
        val account = this.repository.getOne(accountId)
        if (isIncome) account.balance = account.balance!!.add(amount) else account.balance = account.balance!!.subtract(amount)
    }

    open fun makeDefault(id: Long) {
        if (this.repository.existsById(id)) {
            this.repository.findAll().forEach {
                it.defaultAccount = it.id == id
            }
        } else {
            throw IllegalArgumentException("There is no account with $id")
        }
    }

    open fun delete(id: Long) {
        this.repository.deleteById(id);
    }
}
