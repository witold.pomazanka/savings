package pl.mulaczos.savings.domain.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.mulaczos.savings.configuration.mapper.ExpenditureMapper;
import pl.mulaczos.savings.domain.dto.AddExpenditureDto;
import pl.mulaczos.savings.domain.dto.ExpenditureDto;
import pl.mulaczos.savings.domain.model.Category;
import pl.mulaczos.savings.domain.repository.CategoryRepository;
import pl.mulaczos.savings.domain.repository.ExpenditureRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExpenditureService {

    private final ExpenditureRepository repository;
    private final CategoryRepository categoryRepository;
    private final AccountService accountService;
    private final ExpenditureMapper mapper;

    @Transactional
    public Long add(AddExpenditureDto dto) {
        var entity = this.mapper.addExpenditureDtoToExpenditure(dto);
        this.accountService.updateBalance(dto.getAccount(), dto.getPrice(), dto.getIncome());
        return this.repository.save(entity).getId();
    }

    public List<ExpenditureDto> findAll() {
        return this.mapper.entityToDto(this.repository.findAllByOrderByEventDateAsc());
    }

    public ExpenditureDto getOne(long id) {
        return this.mapper.entityToDto(this.repository.getOne(id));
    }

    @Transactional
    public void delete(Long id) {
        var expenditure = this.repository.getOne(id);
        this.accountService.updateBalance(expenditure.getAccount().getId(), expenditure.getPrice(), !expenditure.getIncome());
        this.repository.delete(expenditure);
    }

    public Map<String, List<ExpenditureDto>> findAllGroupByCategory(LocalDate since, LocalDate until) {
        final List<Category> categories = this.categoryRepository.findAllByOrderByPositionAsc();
        long accountId = this.accountService.getDefaultAccount().getId();
        var data = new HashMap<String, List<ExpenditureDto>>();
        categories.forEach(category -> {
            final List<ExpenditureDto> expenditureDtos =
                this.mapper.entityToDto(this.repository.findAllByCategoryAndAccountIdAndEventDateGreaterThanEqualAndEventDateLessThanEqualOrderByEventDateDesc(
                    category, accountId, since, until));
            if (!expenditureDtos.isEmpty()) {
                data.put(category.getName(), expenditureDtos);
            }
        });
        return data;
    }

    @Transactional
    public void update(AddExpenditureDto dto) {
        var expenditure = this.repository.getOne(dto.getId());
        this.accountService.updateBalance(expenditure.getAccount().getId(), expenditure.getPrice(), !expenditure.getIncome());
        expenditure = this.mapper.addExpenditureDtoToExpenditure(dto);
        this.accountService.updateBalance(dto.getAccount(), dto.getPrice(), dto.getIncome());
        this.repository.save(expenditure);
    }
}
