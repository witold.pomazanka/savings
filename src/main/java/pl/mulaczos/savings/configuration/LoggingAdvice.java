package pl.mulaczos.savings.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class LoggingAdvice {

    @Around("@within(org.springframework.web.bind.annotation.RestController)")
    public Object logControllerMethodWithBodyAndReturnedData(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        ObjectMapper mapper = new ObjectMapper();
        log.info("Invoking method: {}", proceedingJoinPoint.getSignature().toShortString().replace("..", mapper.writeValueAsString(proceedingJoinPoint.getArgs())));
        Object proceeded = proceedingJoinPoint.proceed();
        log.info("Returning data: {}", mapper.writeValueAsString(proceeded));
        return proceeded;
    }
}
