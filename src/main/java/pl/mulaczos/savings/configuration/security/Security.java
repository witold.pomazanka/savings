package pl.mulaczos.savings.configuration.security;

import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class Security extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;
    private final RestAccessDeniedHandler restAccessDeniedHandler;
    private final RestUnauthorizedEntryPoint restUnauthorizedEntryPoint;
    private final SecurityAuthenticationFailureHandler securityAuthenticationFailureHandler;
    private final SecurityAuthenticationSuccessHandler securityAuthenticationSuccessHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .headers().disable()
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/swagger-ui.html").permitAll()
            .anyRequest().permitAll()
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(restUnauthorizedEntryPoint)
            .accessDeniedHandler(restAccessDeniedHandler)
            .and()
            .formLogin()
            .permitAll()
            .failureHandler(securityAuthenticationFailureHandler)
            .successHandler(securityAuthenticationSuccessHandler)
            .and()
            .logout()
            .logoutUrl("/api/logout")
            .clearAuthentication(true)
            .invalidateHttpSession(true)
            .deleteCookies()
            .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK))
            .and()
            .formLogin()
            .loginPage("/api/login");
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        JdbcDaoImpl jdbcImpl = new JdbcDaoImpl();
        jdbcImpl.setDataSource(dataSource);
        jdbcImpl.setUsersByUsernameQuery("select username,password, enabled from users where username=?");
        jdbcImpl.setAuthoritiesByUsernameQuery("select b.username, a.role from authorities a, users b where b.username=? and a.user_id=b.id");
        return jdbcImpl;
    }

    @Bean
    public PasswordEncoder passwordencoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordencoder());
    }
}
