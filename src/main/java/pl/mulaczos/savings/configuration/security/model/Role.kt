package pl.mulaczos.savings.configuration.security.model

enum class Role {
    ROLE_ADMIN, ROLE_MODERATOR, ROLE_USER
}