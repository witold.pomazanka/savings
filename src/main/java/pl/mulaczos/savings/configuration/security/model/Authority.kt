package pl.mulaczos.savings.configuration.security.model

import pl.mulaczos.savings.domain.model.BaseEntity
import javax.persistence.*

@Entity
@Table(name = "AUTHORITIES")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "AUTHORITIES_ID_SEQ", allocationSize = 1)
data class Authority(@Enumerated(EnumType.STRING) var role: Role?) : BaseEntity() {
    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null)
}