package pl.mulaczos.savings.configuration.security.model

import pl.mulaczos.savings.domain.model.BaseNamedEntity
import javax.persistence.*

@Entity
@Table(name = "USERS")
@SequenceGenerator(name = "DEFAULT_GEN", sequenceName = "USERS_ID_SEQ", allocationSize = 1)
data class User(var username: String?,
                var password: String?,
                var lastname: String?,
                var email: String?,
                var enabled: Boolean?,
                @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST], mappedBy = "id")
                var authorities: List<Authority>?) : BaseNamedEntity() {
    //NECESSARY FOR MAPSTRUCT
    constructor() : this(null, null, null, null, null, null)
}