package pl.mulaczos.savings.configuration.mapper

import org.mapstruct.Mapper
import org.mapstruct.ReportingPolicy
import pl.mulaczos.savings.domain.dto.LabelDto
import pl.mulaczos.savings.domain.model.Label

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface LabelMapper : BaseMapper<Label, LabelDto>