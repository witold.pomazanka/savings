package pl.mulaczos.savings.configuration.mapper

import org.mapstruct.Mapper
import org.mapstruct.NullValuePropertyMappingStrategy.SET_TO_NULL
import org.mapstruct.ReportingPolicy
import pl.mulaczos.savings.domain.dto.AddCategoryDto
import pl.mulaczos.savings.domain.dto.CategoryDto
import pl.mulaczos.savings.domain.model.Category

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = SET_TO_NULL)
interface CategoryMapper : BaseMapper<Category, CategoryDto> {
    fun addCategoryToCategory(data: AddCategoryDto): Category
}