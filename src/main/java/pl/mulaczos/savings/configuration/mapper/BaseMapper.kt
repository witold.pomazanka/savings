package pl.mulaczos.savings.configuration.mapper

interface BaseMapper<E, D> {
    fun entityToDto(entity: E): D
    fun entityToDto(entity: List<E>?): List<D>?
    fun dtoToEntity(dto: D): E
    fun dtoToEntity(entity: List<D>): List<E>
}