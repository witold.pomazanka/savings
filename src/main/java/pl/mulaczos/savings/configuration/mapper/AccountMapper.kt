package pl.mulaczos.savings.configuration.mapper

import org.mapstruct.Mapper
import org.mapstruct.NullValuePropertyMappingStrategy
import org.mapstruct.ReportingPolicy
import pl.mulaczos.savings.domain.dto.AccountDto
import pl.mulaczos.savings.domain.dto.AddAccountDto
import pl.mulaczos.savings.domain.model.Account

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_NULL)
abstract class AccountMapper : BaseMapper<Account, AccountDto> {
    fun addAccountDtoToAccount(data: AddAccountDto): Account {
        val account = Account(data.balance, false)
        account.name = data.name
        return account
    }
}
