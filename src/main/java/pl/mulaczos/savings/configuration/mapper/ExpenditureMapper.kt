package pl.mulaczos.savings.configuration.mapper

import org.mapstruct.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import pl.mulaczos.savings.domain.constants.ApplicationConstants.NO_CATEGORY_KEY
import pl.mulaczos.savings.domain.dto.AddExpenditureDto
import pl.mulaczos.savings.domain.dto.ExpenditureDto
import pl.mulaczos.savings.domain.model.Expenditure
import pl.mulaczos.savings.domain.model.ExpenditureLabel
import pl.mulaczos.savings.domain.model.Label
import pl.mulaczos.savings.domain.repository.AccountRepository
import pl.mulaczos.savings.domain.repository.CategoryRepository
import pl.mulaczos.savings.domain.repository.LabelRepository

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.WARN)
abstract class ExpenditureMapper : BaseMapper<Expenditure, ExpenditureDto> {

    @Autowired
    var categoryRepository: CategoryRepository? = null
    @Autowired
    var accountRepository: AccountRepository? = null
    @Autowired
    var labelRepository: LabelRepository? = null

    val defaultCategoryName = NO_CATEGORY_KEY

    @Mappings(
            Mapping(target = "category", expression = "java(data.getCategory() == null ? getCategoryRepository().findByName(getDefaultCategoryName()) : getCategoryRepository().getOne(data.getCategory()))"),
            Mapping(target = "account", expression = "java(getAccountRepository().getOne(data.getAccount()))"),
            Mapping(source = "income", target = "income", defaultValue = "false"),
            Mapping(source = "eventDate", target = "eventDate", defaultExpression = "java(java.time.LocalDate.now())")
    )
    abstract fun addExpenditureDtoToExpenditure(data: AddExpenditureDto): Expenditure

    @IterableMapping(qualifiedByName = ["stringToExpenditureLabel"])
    abstract fun stringsToExpenditureLabels(data: List<String>): List<ExpenditureLabel>

    @Transactional
    fun stringToExpenditureLabel(data: String): ExpenditureLabel {
        if (!labelRepository!!.existsByName(data)) {
            val newLabel = Label()
            newLabel.name = data
            labelRepository!!.save(newLabel)
        }
        val expenditureLabel = ExpenditureLabel()
        expenditureLabel.name = data
        return expenditureLabel
    }

    @IterableMapping(qualifiedByName = ["expenditureLabelToString"])
    abstract fun expenditureLabelsToString(data: List<ExpenditureLabel>): List<String>

    fun expenditureLabelToString(data: ExpenditureLabel): String {
        return data.name!!
    }
}
