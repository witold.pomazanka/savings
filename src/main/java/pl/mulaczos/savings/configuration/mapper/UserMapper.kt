package pl.mulaczos.savings.configuration.mapper

import org.mapstruct.IterableMapping
import org.mapstruct.Mapper
import org.mapstruct.ReportingPolicy
import pl.mulaczos.savings.configuration.security.model.Authority
import pl.mulaczos.savings.configuration.security.model.Role
import pl.mulaczos.savings.configuration.security.model.User
import pl.mulaczos.savings.domain.dto.UserDto

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
interface UserMapper : BaseMapper<User, UserDto> {

    @IterableMapping(qualifiedByName = ["authorityToString"])
    fun authoritiesToStrings(data: List<Authority>): List<String>

    fun authorityToString(data: Authority): String {
        return data.role!!.name
    }

    @IterableMapping(qualifiedByName = ["stringToAuthority"])
    fun stringsToAuthorities(data: List<String>): List<Authority>

    fun stringToAuthority(data: String): Authority {
        return Authority(Role.valueOf(data))
    }
}